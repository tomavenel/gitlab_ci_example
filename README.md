Gitlab CI example [![build status](https://gitlab.com/tomavenel/gitlab_ci_example/badges/master/build.svg)](https://gitlab.com/tomavenel/gitlab_ci_example/commits/master)
==================================================================================================================================================================================================

An example `.gitlab-ci.yml` configuration file for Gitlab Continuous Integration tool for a simple Java project built using Gradle.
